import React, { useState } from 'react';
import useTimer from '../hooks/useTimer';
import { millisecondsToMinutes } from "../../utils/timeUtils";
import { connect } from 'react-redux';
import { addLapAndAlert } from "../../actions";
import "./timer.css"
import LapsList from "./lapsList/lapsList";


const Timer = (props) => {
    const [milliseconds, start, stop, isRunning] = useTimer();
    const [isLapsListShown, setIsLapsListShown] = useState(false);

    const handleStartAndStopClick = () => {
        isRunning ? stop() : start();
    }

    const handleLapClick = () => {
        props.addLap(milliseconds, props.lapAlertDuration);
    }

    const handleShowAndHideLapsClick = () => {
        setIsLapsListShown(prevState => !prevState);
    }

    return (
        <div className="timer-page">
            <div className="timer-container">
                <div className={"monitor " + (props.lapAlert ? "last-lap-mode" : "timer-mode")}>
                    {props.lapAlert
                        ? millisecondsToMinutes(props.lapAlert)
                        : millisecondsToMinutes(milliseconds)}
                </div>
                <div className="control-buttons">
                    <button onClick={handleStartAndStopClick}>
                        {isRunning ? 'stop' : "start"}
                    </button>
                    <button onClick={handleLapClick}
                            disabled={!isRunning}
                    >
                        lap
                    </button>
                    <button onClick={handleShowAndHideLapsClick}
                            disabled={!props.laps.size}
                    >
                        {isLapsListShown ? "hide laps" : "show laps"}
                    </button>
                </div>
            </div>
            <div className="timer-laps">
                {isLapsListShown &&
                <LapsList laps={props.laps}/>
                }
            </div>
        </div>
    )
};

const mapStateToProps = (state) => ({
    laps: state.get("laps"),
    lapAlert: state.get("lapAlert")
});

const mapDispatchToProps = (dispatch) => ({
    addLap: (time, lapAlertDuration) => dispatch(addLapAndAlert(time, lapAlertDuration)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Timer);
