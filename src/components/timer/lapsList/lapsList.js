import React from 'react';
import { millisecondsToMinutes } from "../../../utils/timeUtils";
import "./lapsList.css";


const LapsList = (props) => {
    return props.laps.map((lap, lapIndex) =>
        <div className="laps-list" key={lapIndex}>{millisecondsToMinutes(lap)}</div>
    )
}

export default LapsList;
