import { useState, useRef } from 'react';


const useTimer = () => {
    const [milliseconds, setMilliseconds] = useState(0);
    const [isRunning, setIsRunning] = useState(false);
    const intervalRef = useRef();

    const start = () => {
        intervalRef.current = setInterval(updateTime, 10);
        setIsRunning(true);
    }

    const stop = () => {
        clearInterval(intervalRef.current);
        intervalRef.current = null;
        setIsRunning(false);
    }

    const updateTime = () => {
        setMilliseconds(prevState => prevState + 10);
    }

    return [milliseconds, start, stop, isRunning];
};

export default useTimer;
