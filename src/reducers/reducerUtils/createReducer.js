export const createReducer = (handlers, initialState) => {
    return (state = initialState, action) => {
        return handlers[action.type] ? handlers[action.type](state, action) : state;
    }
}