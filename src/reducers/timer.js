import { List } from 'immutable';
import { createReducer } from "./reducerUtils/createReducer";


const defaultLapsState = List();

const lapsHandlers = {
    ADD_LAP: (state, action) => state.push(action.time),
}

export const laps = createReducer(lapsHandlers, defaultLapsState)

const lapAlertHandlers = {
    SET_LAP_ALERT: (state, action) => action.time,
    CLEAR_LAP_ALERT: () => null
}

export const lapAlert = createReducer(lapAlertHandlers, null);