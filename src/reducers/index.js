import { combineReducers } from 'redux-immutable';
import { laps, lapAlert } from './timer';

export default combineReducers({
    laps,
    lapAlert
});