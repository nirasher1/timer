const numberToNDigits = (stringNumber, digitsCount = 2) => {
    if (stringNumber.length < digitsCount) {
        return '0'.repeat(digitsCount - stringNumber.length) + stringNumber;
    }
    return stringNumber;
}

export const millisecondsToMinutes = (duration) => {
    const minutes = numberToNDigits(
        Math.floor(duration / 1000 / 60).toString()
    );
    const seconds = numberToNDigits(
        Math.floor(duration / 1000 % 60).toString()
    );
    const milliseconds = numberToNDigits(
        Math.floor(duration % 1000).toString(), 3
    );
    return `${minutes}:${seconds}:${milliseconds}`;
}