import React from 'react';
import Config from './config';
import Timer from './components/timer/timer';
import './App.css';

function App() {
  return (
    <div className="page">
      <Timer lapAlertDuration={Config.lapAlertDuration}
      />
    </div>
  );
}

export default App;
