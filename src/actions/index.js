const addLap = (time) => ({
    type: 'ADD_LAP',
    time
})

const setLapAlert = (time) => ({
    type: 'SET_LAP_ALERT',
    time
})

const clearLapAlert = () => ({
    type: 'CLEAR_LAP_ALERT'
})

export const addLapAndAlert = (time, alertDuration) => (
    (dispatch) => {
        dispatch(addLap(time));
        dispatch(setLapAlert(time));
        setTimeout(() => dispatch(clearLapAlert()), alertDuration)
    }
)